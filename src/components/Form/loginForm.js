import React, { useState } from "react";

const LoginForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [allEntry, setAllEntry] = useState([]);

  const submitForm = (e) => {
    e.preventDefault();
    const newEntry = { email: email, password: password };
    setAllEntry([...allEntry, newEntry]);
    // console.log(allEntry);
  };
  
  
  return (
    <>
      <form  onSubmit={submitForm}>
        <div>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            name="email"
            id="email"
            autoComplete="off"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        
        <br />

        <div>
          <label htmlFor="password">Password:</label>
          <input
            type="password"
            name="password"
            id="password"
            autoComplete="off"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <br />
        <button type="submit">Log in</button>
      </form>

      <div>
        {allEntry.map((currElem, index) => {
          return (
            <div className="showDataStyle" key={index}>
              <p>{currElem.email}</p>
              <p>{currElem.password}</p>
            </div>
          );
        })}
      </div>
    </>
  );
};
export default LoginForm;

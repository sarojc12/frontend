import React, { useState } from 'react';

const ReadCount = () =>{

    const [count,setCount] = useState(0);
    const [inputNumber, setInputNumber] = useState(1)

    const increment = () => setCount(count + inputNumber)
    const decrement = () => setCount(count - inputNumber) 
    const reset = () => {
        setCount(0)
        setInputNumber(1)
    }

    return(
        <>
        <p>Count: {count}</p>
        <input type="number" value={inputNumber} onChange={(e) => setInputNumber(parseFloat(e.target.value))} />
        <div>
            <button onClick={increment}>Add</button>
            <button onClick={reset}>Reset</button>
            <button onClick={decrement}>Sub</button>
        </div>
        </>
    );

};
export default ReadCount;